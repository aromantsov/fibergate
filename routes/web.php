<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::resource('/work', App\Http\Controllers\WorkController::class);
Route::get('/import', [App\Http\Controllers\WorkController::class, 'import'])->name('import');
Route::post('/upload', [App\Http\Controllers\WorkController::class, 'upload'])->name('upload');
Route::get('/currency', [App\Http\Controllers\CurrencyController::class, 'index'])->name('currency');
Route::post('/transfer', [App\Http\Controllers\CurrencyController::class, 'transfer'])->name('transfer');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
