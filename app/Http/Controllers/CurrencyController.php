<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
        return view('currency');
    }

    public function transfer(Request $request)
    {
    	$messages = [
            'required' => 'Поле сумма должно быть заполнено',
            'min' => 'Минимальная сумма - 1',
            'numeric' => 'Поле Сумма должно быть числом',
            'different' => 'Выбранные валюты должны отлиаться'
        ];

        $validator = $this->validate($request, [
            'currencyCodeВ' => 'different:currencyCodeA',
            'sum' => 'required|numeric|min:1',
        ], $messages);

        $courses = json_decode(file_get_contents('https://api.monobank.ua/bank/currency'));

        $rate = $this->setRate($request['currencyCodeA'], $request['currencyCodeB'], $courses);

        $result = $request['sum'] * $rate;

        echo $result;
    }

    private function setRate($curA, $curB, $courses)
    {
    	foreach($courses as $course){
    		if(($curA == $course->currencyCodeA) && ($curB == $course->currencyCodeB)){
    			if(isset($course->rateBuy)){
    				$rate = $course->rateBuy;
    			}else{
    				$rate = $course->rateCross;
    			}
    			return $rate;
    		}elseif(($curA == $course->currencyCodeB) && ($curB == $course->currencyCodeA)){
    			if(isset($course->rateSell)){
    				$rate = 1 / $course->rateSell;
    			}else{
    				$rate = 1 / $course->rateCross;
    			}
    			return $rate;
    		}
    	}

    	foreach($courses as $course){
    		if(($curA == $course->currencyCodeA) && (980 == $course->currencyCodeB)){
    			if(isset($course->rateBuy)){
    				$rate1 = $course->rateBuy;
    			}else{
    				$rate1 = $course->rateCross;
    			}
    		}
    	    if((980 == $course->currencyCodeB) && ($curB == $course->currencyCodeA)){
    			if(isset($course->rateSell)){
    				$rate2 = 1 / $course->rateSell;
    			}else{
    				$rate2 = 1 / $course->rateCross;
    			}
    		}
    	}
 
    	$rate = $rate2 * $rate1;
    	return $rate;
    }
}
