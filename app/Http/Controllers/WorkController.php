<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Work;
use App\Models\User;
use App\Models\Status;
use App\Imports\WorkImport;

class WorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('works.index', [
            'works' => Work::with('users')->paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('works.create', [
                'work' => [],
                'users' => User::all(),
                'statuses' => Status::all()
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
                'required' => 'Все поля должны быть заполнены',
                'max' => 'Максимальная длина поля 191 символ'
            ];

            $validator = $this->validate($request, [
                'task' => 'required|string|max:191',
                'user_id' => 'required',
                'deadline' => 'required|date',
                'status' => 'required'
            ], $messages);

        $work = Work::create($request->all());

        request()->session()->flash('success', 'Задача успешно создана');
        return redirect()->route('work.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Work $work)
    {
        return view('works.edit', [
                'work' => $work,
                'users' => User::all(),
                'statuses' => Status::all()
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Work $work)
    {
        $messages = [
                'required' => 'Все поля должны быть заполнены',
                'max' => 'Максимальная длина поля 191 символ'
            ];

            $validator = $this->validate($request, [
                'task' => 'required|string|max:191',
                'user_id' => 'required',
                'deadline' => 'required|date',
                'status' => 'required'
            ], $messages);

        $work->task = $request['task'];
        $work->user_id = $request['user_id'];
        $work->deadline = $request['deadline'];
        $work->status = $request['status'];
        $work->save();

        request()->session()->flash('success', 'Задача успешно обновлена');
        return redirect()->route('work.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Work $work)
    {
        $work->delete();
        request()->session()->flash('success', 'Задача успешно удалена');
        return redirect()->route('work.index');
    }

    public function import()
    {
        return view('works.import');
    }

    public function upload(Request $request)
    {
        $validator = $this->validate($request, [
            'file' => 'required|max:100000|mimes:xlsx,xls,csv'
        ]);

        if($validator){
            \Excel::import(new WorkImport, request()->file('file'));
            return redirect()->back()->with(['success' => 'Файл успешно импортирован']);
        }else{
            return redirect()->back()->with(['errors' => $validator->errors()->all()]);
        }
    }
}
