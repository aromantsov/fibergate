<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Work;
use App\Models\User;
use App\Models\Status;
use Carbon\Carbon;

class WorkImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
    	$i = 0;
    	foreach($collection as $row){
    		if($i == 0){
    			$i++;
    			continue;
    		}
    		Work::create([
    		    'task' => $row[0],
                'user_id' => $this->setUser($row[1]),
                'deadline' => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date:: excelToDateTimeObject($row[2])),
                'status' => $this->setStatus($row[3])
    		]);
    		$i++;
    	}
    }

    private function setUser($name){
        $user = User::where('name', $name)->first();

        if($user){
        	return $user->id;
        }

        $new_user = User::create([
            'name' => $name,
            'email' => rand(0, 250000) . '@i.com',
            'password' => ''
        ]);

        return $new_user->id;
    }

    private function setStatus($status){
        $status = Status::where('status', $status)->first();

        if($status){
        	return $status->id;
        }

        return 1;
    }
}
