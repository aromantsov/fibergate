@extends('layouts.app')

@section('content')

<div class="container">
	<hr/>
	<form action="{{ route('work.store') }}" method="post" class="form-horizontal">
		{{ csrf_field() }}
		@include('works.partials.form')
	</form>
</div>

@endsection