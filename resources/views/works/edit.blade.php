@extends('layouts.app')

@section('content')

<div class="container">
	<hr/>
	<form action="{{ route('work.update', $work) }}" method="post" class="form-horizontal">
		<input type="hidden" name="_method" value="PUT"> 
		{{ csrf_field() }}
		@include('works.partials.form')
	</form>
</div>

@endsection