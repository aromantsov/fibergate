@extends('layouts.app')

@section('content')
<div class="container">
  <hr/>
  @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
  <a href="/work/create" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"> Создать</i></a>
  <a href="{{ route('import') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"> Импорт</i></a>
  <table class="table table-striped">
    <thead>
      <th>Задача</th>
      <th>Ответственный</th>
      <th class="text-right">Действия</th>
    </thead>
    <tbody>
      @forelse($works as $work)
      <tr>
        <td>{{ $work->task }}</td>
        <td>{{ $work->users->name }}</td>
        <td class="text-right">
            <form action="{{ route('work.destroy', $work) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
                  <input type="hidden" name="_method" value="DELETE"> 
                  {{ csrf_field() }}
                  <a href="{{ route('work.edit', $work) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                  <button type="submit" class="btn"><i class="fa fa-trash"></i></button>
                </form></td>
      </tr>
      @empty
      <tr>
        <td colspan="3" class="text-center">Нет задач</td>
      </tr>
      @endforelse
    </tbody>
    <tfoot>
      <tr>
        <td colspan="3">
          <ul class="pagination full-right">
            {{ $works->links() }}
          </ul>
        </td>
      </tr>
    </tfoot>
  </table>
</div>

@endsection