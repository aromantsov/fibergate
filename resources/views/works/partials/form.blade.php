
<label for="">Задача</label>
<input type="text" class="form-control" name="task" placeholder="Задача" value="{{ $work->task ?? '' }}" required>
<label for="">Ответственный</label>
<select name="user_id" id="" class="form-control" required>
	<option value="0" disabled @if(!isset($work->user_id)) selected @endif>--Ответственный--</option>
	@foreach($users as $user)
    <option value="{{ $user->id }}" @if((isset($work->user_id)) && ($work->user_id == $user->id))selected @endif>{{ $user->name }}</option>
    @endforeach
</select>

<label for="">Срок выполнения</label>
    <input type="date" class="form-control" name="deadline" placeholder="" value="{{ $work->deadline ?? '' }}" required>

<label for="">Статус</label>
<select name="status" id="" class="form-control" required>
    @foreach($statuses as $status)
    <option value="{{ $status->id }}" @if((isset($work->status)) && ($work->status == $status->id))selected @endif>{{ $status->status }}</option>
    @endforeach
</select>    

<hr/>
<input type="submit" class="btn btn-primary">    