@extends('layouts.app')

@section('content')
<div class="container">
  <hr/>
  <form action="" class="form-horizontal" id="form">
    {{ csrf_field() }}
<!--     <label for="">Задача</label>
    <input type="text" class="form-control" name="task" placeholder="Задача" value="{{ $work->task ?? '' }}" required> -->
    <label for="">Наальная валюта</label>
    <select name="currencyCodeA" id="" class="form-control" required>
    <option value="980">Украинская гривна</option>
    <option value="643">Российский рубль</option>
    <option value="840">Доллар США</option>
    <option value="978">Евро</option>
    </select>
    
    <label for="">Конечная валюта</label>
    <select name="currencyCodeB" id="" class="form-control" required>
    <option value="980">Украинская гривна</option>
    <option value="643">Российский рубль</option>
    <option value="840">Доллар США</option>
    <option value="978">Евро</option>
    </select>

    <label for="">Сумма</label>
    <input type="number" class="form-control" name="sum" placeholder="Сумма" value="" required> 

    <hr/>
    <button type="button" class="btn btn-primary" onclick="calculate();">Подситать</button>    
    </form>
    <div id="result"></div>
    </div>
    <script>
      function calculate(){
        $.ajax({
          url: '/transfer',
          method: 'post',
          data: $('form').serialize(),
          success: function(result){
            $('#result').html(result);
          }
        });
      }
    </script>
    @endsection