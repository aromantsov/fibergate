@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
   	   	<div class="col-sm-3">
   	   		<div class="jumbotron">
   	   			<a href="/work"><span class="label label-primary">Дела</span></a>
   	   		</div>
   	   	</div>
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="{{ route('currency') }}"><span class="label label-primary">Валюты</span></a>
               </div>
            </div>
   	   </div>
</div>
@endsection
